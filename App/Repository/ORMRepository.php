<?php 

namespace App\Repository;
require_once("connection.php");
use App\Entity\ORMEntity;
use ReflectionClass;

abstract class ORMRepository{

    public $connection;

    public function __construct()
    {
        $bdd = new \connection();
        $this->connection = $bdd->getInstance();
    } 

    function getTable(){
        $entity = static::ENTITY;
        $reflect = new ReflectionClass($entity);
        return $reflect->getConstant("TABLE_NAME");
    }

    function getPrimary(){
        $entity = static::ENTITY;
        $reflect = new ReflectionClass($entity);
        return $reflect->getConstant("PRIMARY_KEY");
    }

    function nbParametre($array){
        $nbParametre = sizeof($array);
        $txt = "?";
        $string = "";
        for($i=1;$i<=$nbParametre;$i++)
        {
            if($i == $nbParametre){
                $string = $string.$txt;
            }
            else{
                $string = $string.$txt.",";
            }
        }
        return $string;
    }

    public function save(ORMEntity $product){
        $db = $this->connection;
        $reflect = new ReflectionClass($product);
        $publicProperties = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
        $getPrimaryKeyName = "get".$product->getPrimaryKey();
        
        $array = [];

        foreach ($publicProperties as $property){
            echo $property->getName();
            if ($property->getValue($product) != "")
            {
                if($property->getName() != $product->getPrimaryKey()){
                    $array[$property->getName()] = $property->getValue($product);
                }              
            }
        }

        if(!is_null($product->{$getPrimaryKeyName}())){
            $propertiesName = implode("=?, ", array_keys($array));
            $propertiesName = $propertiesName."=?";
            $req = $db->prepare("UPDATE ".$product->getTableName()." SET ".$propertiesName." WHERE ".$product->getPrimaryKey()." = ".$product->{$getPrimaryKeyName}());
        }
        else
        {
            $propertiesName = implode(", ", array_keys($array));
            $req = $db->prepare("INSERT INTO ".$product->getTableName()." (".$propertiesName.") VALUES (".$this->nbParametre($array).")");
        }
        
        $req->execute(array_values($array));
    }

    public function get(int $id){
        $db = $this->connection;

        $req = $db->prepare("SELECT * FROM ".$this->getTable()." WHERE ".$this->getPrimary()." = :id");
        $req->execute(array(
            'id' => $id
        ));

        $req = $req->fetch(\PDO::FETCH_ASSOC);      
        $keys = array_keys($req);

        $entity = static::ENTITY;
        $product = new $entity();
        foreach($keys as $key){
            $setter = "set".$key;
            if(!is_null($req[$key]))
            {
                $product->{$setter}($req[$key]);
            }
        }    

        return $product;
    }

    public function getList(){
        $db = $this->connection;

        $entity = static::ENTITY;

        $req = $db->prepare("SELECT * FROM ".$this->getTable());
        $req->execute();

        $req = $req->fetchAll(\PDO::FETCH_ASSOC);
        
        $keys = array_keys($req);


        $array = array();
        
        foreach($keys as $key)
        {
            $product = new $entity();
            $values = array_keys($req[$key]);
            foreach($values as $value){
                $setter = "set".$value;
                if(!is_null($req[$key][$value]))
                {
                    $product->{$setter}($req[$key][$value]);
                }
            }
            $array[] = $product; 
        }

        return $array;
    }   

    public function delete(ORMEntity $product){
        $db = $this->connection;

        $getPrimaryKeyName = "get".$primaryKey;
        
        $req = $db->prepare("DELETE FROM ".$product->getTableName()." WHERE ".$product->getPrimaryKey()." = :id");
        $req->execute(array(
            'id' => $product->{$getPrimaryKeyName}()
        ));
    } 

    public function deleteById(int $id){
        $db = $this->connection;

        $req = $db->prepare("DELETE FROM ".$this->getTable()." WHERE ".$this->getPrimary()." = :id");
        $req->execute(array(
            'id' => $id
        ));
    } 

    function getProps($pe){
        $reflect = new \ReflectionClass($pe);
        $props   = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);

        foreach ($props as $prop) {
            echo $prop->getName() . "\n";
        }
    }

    function migrationTable(){

        $db = $this->connection;

        $req = $db->prepare("CREATE TABLE ".$this->getTable()." ()");
        $req->execute();

    }

}

?>