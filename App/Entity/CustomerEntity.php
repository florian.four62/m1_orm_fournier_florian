<?php 

namespace App\Entity;

class CustomerEntity implements ORMEntity{

    const TABLE_NAME = "customer";
    const PRIMARY_KEY = "id";

    private $id;
    public $name;
    public $price;

    public function getTableName()
    {
        return self::TABLE_NAME;
    }

    public function getPrimaryKey()
    {
        return self::PRIMARY_KEY;
    }

    function getId(){
        return $this->id;
    }

    function setId(string $id){
        $this->id = $id;
    }

    function getName(){
        return $this->name;
    }

    function setName(string $name){
        $this->name = $name;
    }

    function getPrice(){
        return $this->price;
    }

    function setPrice(int $price){
        $this->price = $price;
    }

}


?>