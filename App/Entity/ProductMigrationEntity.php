<?php 

namespace App\Entity;

class ProductMigrationEntity implements ORMEntity{

    const TABLE_NAME = "productMigration";
    const PRIMARY_KEY = "id";

    private $id;
    public $name;
    public $prix;
    public $stock;

    public function getTableName()
    {
        return self::TABLE_NAME;
    }

    public function getPrimaryKey()
    {
        return self::PRIMARY_KEY;
    }

    function getId(){
        return $this->id;
    }

    function setId(string $id){
        $this->id = $id;
    }

    function getName(){
        return $this->name;
    }

    function setName(string $name){
        $this->name = $name;
    }

    function getPrix(){
        return $this->prix;
    }

    function setPrix(int $prix){
        $this->prix = $prix;
    }

    function getStock(){
        return $this->stock;
    }

    function setStock(int $stock){
        $this->stock = $stock;
    }

}


?>