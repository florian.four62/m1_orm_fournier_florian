<?php 

require_once("Autoload.php");
//use App\Entity\ProductEntity;
//use App\Repository\ProductRepository;

use App\Entity\CustomerEntity;
use App\Repository\CustomerRepository;

Autoload::register();

//$pe = new ProductEntity();
//$rep = new ProductRepository();

// Test d'insertion en base
// ------------------------
//$pe->setName("Insert");
//$rep->save($pe);

// Test de récupération d'un objet
// -------------------------------
//$enregistrement = $rep->get(5);
//var_dump($enregistrement);

// Test de modification de l'enregistrement recupéré
// -------------------------------------------------
//$enregistrement->setName("REZzzz");
//$rep->save($enregistrement);
//var_dump($enregistrement);

// Liste d'objet 
// -------------
/*$listObjets = $rep->getList();

foreach($listObjets as $objet){
    var_dump($objet);
}*/

// Test suppression by id
// ----------------------
//$rep->deleteById(10);

// Test suppression by objet
// -------------------------
//$enregistrement = $rep->get(9);
//$rep->delete($enregistrement);

/*****************/
/*   Customer    */
/*****************/
//$ce = new CustomerEntity();
//$rep = new CustomerRepository();

// Test d'insertion en base
// ------------------------
/*$ce->setName("Insert");
$rep->save($ce);*/

?>