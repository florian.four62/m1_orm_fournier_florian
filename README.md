# M1_ORM_Fournier_Florian

On a :
    - Une page index.php qui est la page d'appel des objets
    - l'Autoload qui vient nous mettre les classes automatiquement dans l'index
    - Un fichier avec l'initialisation de la connection (Connection.php)
    - Un dossier APP avec :
        - Un dossier entity ou l'on retrouve :
            - Les objets avec leurs attributs, setter, getter et leurs constantes pour le nom de la table et de la clé primaire
            - Un ORMEntity qui est une interface que l'on implemente dans les classes entity
        - Un dossier repository ou l'on retrouve :
            - Les classes repository des objets ou il y a simplement la constante de l'entité de la classe que l'on extend à l'ORMRepository
            - L'ORMRepository qui est une classe abstraite et qui regroupe toutes les fonctions :
                - Save qui créé un enregistrement ou le modifie s'il existe déja par rapport à un objet
                - Le get qui récupère un enregistremement par rapport à un identifiant
                - Le getList qui récupère la liste des enregistrements de l'objet
                - Le delete qui supprime l'enregistrement par rapport à un objet
                - Le deleteBy id qui supprimer l'enregistrement par rapport à un identifiant
                - Le getProps qui récupère les propriétés d'un objet 
